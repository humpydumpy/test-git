var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('passport'); // PASSPORT

const session = require('express-session'); // PASSPORT
var bodyParser = require('body-parser'); // PASSPORT

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var bicicletasRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuarios');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var juegoRouter = require('./routes/juego');

const store = new session.MemoryStore; //PASSPORT almacenamiento de la session

var app = express();

// Passport config
require('./config/passport')(passport);


//MONGODB Y MONGOOSE
var mongoose = require('mongoose');
var mongoDB = 'mongodb://nodeuser:cd240nodeuser@127.0.0.1/red_bicicletas?authSource=admin'; //AUTHENTICACION EN MONGO
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongoddb connection error'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//SE URILIZA LA SESION PARA DARLE PERSISTENCIA AL LOGIN UNA VEZ ES CONSEGUIDO
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store, // COOKIE ALMACENADA EN MEMORIA DEL SERVIDOR
  saveUninitialized: true,
  resave: true,
  secret: 're_dsaujdkshjkjklhjdkslah78978'
}));

app.use(bodyParser.urlencoded({ extended: false })); // PASSPORT
app.use(cookieParser());
app.use(passport.initialize()); //PASSPORT
app.use(passport.session()); //PASSPORT


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/usuarios', usuariosRouter);
app.use('/juego', juegoRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// db.createUser(
//   {
//     user: "admin",
//     pwd: "cd240mongo",
//     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
//   }
// )
// db.createUser(
//   {
//     user: "nodeuser",
//     pwd: "cd240nodeuser",
//     roles: [ { role: "readWrite", db: "red_bicicletas" } ]
//   }
// )
module.exports = app;
