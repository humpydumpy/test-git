var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmation_get: function(req, res, next){
        Token.findOne({ token: req.params.token }, function(err, token){
            if(!token){ return res.status(400).send({ type: 'no-verificado',  msg: 'ATRAS! No se vuelva a acercar usted a mis TOKENS!'})}
            Usuario.findById( token._usuarioId, function(err, usuario){
                if(!usuario){ return res.status(400).send({ msg: 'Usuario no encontrado al cruzar datos con el TOKEN especificado'})}
                if(usuario.verificado){ return res.redirect('/juego') } //Si ya existe el usuario verificado, ignoramos la peticion y redireccionamos a LOGIN
                usuario.verificado = true;
                usuario.save(function(err){
                    if(err){ return res.status(500).send({ msg: err.message }) }
                    res.redirect('/');
                });
            });
        });
    }
};