const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicicletas){
        res.status(200).json({
            bicicletas: bicicletas
        });
    });
};


exports.bicicleta_create = function(req, res){
    // var bici = new Bicicleta({
    //     code: req.body.id, 
    //     color: req.body.color, 
    //     modelo: req.body.modelo,
    //     ubicacion: [req.body.lat, req.body.lat] 
    // });
    // bici.save(function(err, bici){
    //     res.status(200).json({
    //         bicicleta: bici
    //     })
    // });
    Bicicleta.create({ 
        code: req.body.code, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lat] }, function (err, bici) {
            if (err) return console.log(err);
            res.status(200).json({
                bicicleta: bici
            })
      });
};

// exports.bicicleta_delete = function(req, res){
//     var bici = Bicicleta.findById(req.body.id);
//     Bicicleta.removeById(req.body.id);

//     res.status(200).send();    
// }

// exports.bicicleta_update = function(req, res){
//     var bici = Bicicleta.findById(req.params.id);
//     bici.id = req.body.id;
//     bici.color = req.body.color;
//     bici.modelo = req.body.modelo;
//     bici.ubicacion = [req.body.lat, req.body.lng];

//     res.status(200).json({
//         bicicleta: bici
//     });
// }