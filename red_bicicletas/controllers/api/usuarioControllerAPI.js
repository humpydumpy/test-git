const Usuario = require('../../models/usuario');

exports.usuario_list = function(req, res){
    Usuario.find({}, function(err, usuarios){
        console.log(usuarios)
        res.status(200).json({
            usuarios: usuarios
        });
    });
};

exports.usuario_create = function(req, res){
    Usuario.create({ 
        nombre: req.body.nombre, 
        email: req.body.email,
        password: req.body.password
     }, function (err, usu) {
            if (err) return res.json({success: false, error: err});
            res.status(200).json({
                usuario: usu
            })
      });
};

// COMPARAR LA CONTRASEÑA EN TREXTO CLARO CON EL VALOR DEL HASH ALMACENADO EN EL DOCUMENT
exports.password_check = function(req, res){
    usuario = Usuario.findOne({ email: req.body.email }).exec(function(err, usuario){
        if (err) return res.status(404).json({ success: false, error: err })
        console.log("nombre de usuario " + usuario.nombre);
        console.log(usuario.validPassword(req.body.passwd));
        res.status(200).send("contraseña correcta");
    });
    // Usuario.findOne({ email: req.body.email }, function(err, usuario){
    //     console.log("paswword: " + usuario.password);
    //     bcrypt.compareSync(this.usuario, usuario.password);
    //     res.status(200).send("OK");
    // });
};

exports.usuario_similar_nombre = function(req, res){
    console.log('Se crea la instancia bart');
    var bart = new Usuario({ nombre: 'bart', email: 'bart@bart.bart' });

    bart.findNombreSimilar(function(err, barts){
        if (err) return res.json({success: false, error: err});
        res.status(200).json({
            similares: barts
        })
    });
};

exports.usuario_buscar = function(req, res){
    var n = req.params.nombre;
    console.log(n);
    Usuario.buscar(n, function(err, usuarios){
        if (err) res.json({ success: false, error: err })
        res.status(200).json({
            usuario: usuarios  
        })
    });
};

// const Usuario = require('../models/usuario');

// var usuario1 = new usuarioSchema({
//     nombre: 'Bartolome',
//     email: 'bartolome@gmail.com',
// });

// usuario1.save(function(err, usuario1){
//     if(err) return handleError(err);
//     console.log(usuario1);
// });
