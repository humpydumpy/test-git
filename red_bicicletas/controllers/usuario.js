const Usuario = require('../models/usuario');

exports.usuario_list = function(req, res){
    Usuario.find({}, function(err, usuarios){
        console.log(usuarios)
        res.status(200).json({
            usuarios: usuarios
        });
    });
};

exports.login = function(req, res){
    res.render('usuarios/create');
};

exports.usuario_create = function(req, res){
    Usuario.create({ 
        nombre: req.body.nombre, 
        email: req.body.email
     }, function (err, usu) {
            if (err) return res.json({success: false, error: err});
            res.status(200).json({
                usuario: usu
            })
      });
};

// const Usuario = require('../models/usuario');

// var usuario1 = new usuarioSchema({
//     nombre: 'Bartolome',
//     email: 'bartolome@gmail.com',
// });

// usuario1.save(function(err, usuario1){
//     if(err) return handleError(err);
//     console.log(usuario1);
// });
