const Bicicleta = require('../models/bicicleta');

exports.bibicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicicletas){
        res.render('bicicletas/index', { bicis: bicicletas });
    });
}

//Renderiza una simple llamada get y lanza la vista correspondiente
exports.bicicleta_crear_get = function(req, res){
    res.render('bicicletas/crear');
}


//Detecta que el formulario se envia mediante POST y hace una inserccion en la BBDD
exports.bicicleta_crear_post = function(req, res){
    var bici = new Bicicleta({
        code: req.body.id, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lat] 
    }); //Se instancia el modela para procesos CRUD
    bici.save(function(err, bici){
        console.log(bici);
    });
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.deleteOne({code: req.body.id}, function(err){});

    res.redirect('/bicicletas')
}


exports.bicicleta_update_get = function(req, res){
    Bicicleta.findOne({code: req.params.id}, function(err, bicicletas){
        res.render('bicicletas/update', { bici: bicicletas });
    });
}

exports.bicicleta_update_post = function(req, res){
    // var bici = Bicicleta.findById(req.params.id);
    Bicicleta.updateOne({code: req.params.id}, {
            code: req.body.id,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat, req.body.lng]
         },
         (err) => {});
    res.redirect('/bicicletas');
}