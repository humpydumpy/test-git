const Usuario = require('../models/usuario');
const Token = require('../models/token');
const passport = require('passport');
const mailer = require('../mailer/mailer');

exports.dashboard_get = function(req, res){
    console.log(req.user.nombre);
    res.render('juego/dashboard', { nombreUsuario : req.user.nombre });
}

exports.index_juego = function(req, res){
    res.render('juego/index');
};

exports.login = function(req, res, next) {
    passport.authenticate('local', function(err, user, info) { // "info" RECOJE EL MESSAGE ENVIADO DESDE PASSPORT.js
        if(req.body.username === '' || req.body.passport === ''){
            info.message = 'Por favor, rellena el formulario';
        };
        if (err) { return next(err); }
        if (!user) { return res.render('juego/index', { mensaje: { msg1: 'Error al hacer Login', msg2: info.message} }); }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        return res.redirect('/juego/dashboard');
      });
    })(req, res, next);
}

exports.loggedIn = function(req, res, next){
    if(req.user){
        next();
    } else {
        console.log('Usuario no loggeado');
        res.redirect('/juego');
    }
}

exports.registar_usuario_get = function(req, res){
    res.render('juego/registrarse' , {errors:{}, usuario: new Usuario()});
}

exports.registrar_usuario_post = function(req, res, next){
    console.log("Contraseña " + req.body.password);
    console.log("Usuario " + req.body.usuario);
    console.log("Email " + req.body.email);

    // FALTA LA SECURIZACION DE LAS CONTRASEÑAs

    if(req.body.password != req.body.repetida_password){
        res.render('juego/registrarse', { errors: { repetida_password: { message: 'Las contraseñas no coinciden.' }}, usuario: new Usuario({ nombre: req.body.usuario, email: req.body.email })});
        return;
    }
    
    Usuario.create({
        nombre: req.body.usuario,
        email: req.body.email,
        password: req.body.password
    }, function(err, nuevoUsuario) {
        console.log('LLEGO AQUI?');
        if(err) { 
            console.log(err.message);
            res.render('juego/registrarse', {errors: err.errors, usuario: new Usuario({ nombre: req.body.usuario, email: req.body.email })});
        } else {
            console.log('Este es el nuevo usuario ' + nuevoUsuario);
            // EMAIL DE BIENVENIDA
            // const mailOptions = {
            //     from: 'admin@pegaso.website',
            //     to: req.body.email,
            //     subject: 'Verificacion de cuenta Quo Sabis',
            //     text: 'El equipo de Quo Sabis te da la bienvenida ' + req.body.usuario + '\nEn breves recibirá un e-mail de confirmación de su cuenta'
            // };
            
            // mailer.sendMail(mailOptions, function(err, info){
            //     if(err){ return console.log(err.message) }
            //     console.log('Se ha enviado un email a ' + req.body.email + '.');
            //     console.log('email info ' + info.messageId  + '.');
            // });
            nuevoUsuario.enviar_email_bienvenida(),       
            res.render('juego/index');        
        }        
    });
};

exports.mostrar_usuarios_get = function(req, res, next){
    Usuario.find({}).
    populate('_tokenID', 'token'). //SE ANEXA LA FOREIGN KEY USANDO POPULATE(CAMPO REF, ATRIBUTO QUE QUIERES TRAER)
    exec(function(err, usuarios){
        if(err){
            if(err) res.render('juego/admin', { error: err.message });
        } else {
            return res.render('juego/admin', { usuarios: usuarios });
        };
    });
    // Usuario.find({}, function(err, usuarios){
    //     if(err) res.render('juego/admin', { error: err.message });
    //     return res.render('juego/admin', { usuarios: usuarios });
    // });
};

exports.usuario_delete_post = function(req, res, next){
    Usuario.deleteOne({ _id: req.params.id }, function(err, usuario){
        if(err){ console.log('Fallo al eliminar usuario' + usuario.nombre) }
        return res.redirect('/juego/admin');
    });
};


