var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://127.0.0.1/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we aRE CONNECTED TO THE DATABASE');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });
    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "purpura", "urbana", [-34.44444, -3.343434]);
    
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("purpura");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.44444);
            expect(bici.ubicacion[1]).toEqual(-3.343434);
        });
    });
        
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
});





// beforeEach(() => { Bicicleta.allBicis = []; });
// beforeEach(() => { console.log("testeando.."); });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una bicicleta', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'rojo', 'urbana', [36.718892, -4.421970]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });


// describe('Bicicleta.findById', () => {
//     it('Buscamos por id una bici con id = 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1, 'rojo', 'urbana', [36.718892, -4.421970]);
//         var b = new Bicicleta(3, 'verde', 'urbana', [36.718892, -4.421970]);
//         Bicicleta.add(a);
//         Bicicleta.add(b);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1); 
//         expect(targetBici.color).toBe(a.color); 
//         expect(targetBici.modelo).toBe(a.modelo); 
//     });
// });

// describe('Bicicleta.removeById', () => {
//     it('borramos por id', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1, 'rojo', 'urbana', [36.718892, -4.421970]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         Bicicleta.removeById(1);
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });