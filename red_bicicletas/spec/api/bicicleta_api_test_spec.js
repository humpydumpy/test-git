var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www'); //Ejecutara el servidor web express hasta la finalizacion del script

describe('Bicicleta API', () => {
    describe('GET STATUS', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [36.718892, -4.421970]);
            Bicicleta.add(a);

            request.get('http://127.0.0.1:8080/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST bicicleta/create', () => {
    it('STATUS 200', (done) => {
        var headers = { 'content-type': 'application/json' };
        var aBici = '{ "id": 10, "color": "rojo", "modelo": "btt", "lat": 34, "lng": 56 }';
        request.post({
            headers: headers,
            url: 'http://127.0.0.1:8080/api/bicicletas/create',
            body: aBici
        }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});