var express = require('express');
var router = express.Router();
var usuarioController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioController.usuario_list);
router.post('/create', usuarioController.usuario_create);
router.get('/similar', usuarioController.usuario_similar_nombre);
router.post('/:nombre/buscar', usuarioController.usuario_buscar);
router.post('/compararpwd', usuarioController.password_check);


module.exports = router;