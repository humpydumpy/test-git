var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bibicleta_list);
router.get('/crear', bicicletaController.bicicleta_crear_get);
router.post('/crear', bicicletaController.bicicleta_crear_post);
router.post('/:id/delete', bicicletaController.bicicleta_delete_post); //comentario de prueba
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);


module.exports = router;