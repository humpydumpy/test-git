var express = require('express');
var router = express.Router();
var juegoController = require('../controllers/juego');
var tokenController = require('../controllers/token');


router.get('/', juegoController.index_juego);
router.get('/registrarse', juegoController.registar_usuario_get);
router.post('/registrarse', juegoController.registrar_usuario_post);
router.post('/', juegoController.login);
router.get('/dashboard', juegoController.loggedIn, juegoController.dashboard_get);
router.get('/admin', juegoController.mostrar_usuarios_get);
router.post('/admin/:id/delete', juegoController.usuario_delete_post);
router.get('/token/confirmation/:token', tokenController.confirmation_get);


module.exports = router;

//holahola