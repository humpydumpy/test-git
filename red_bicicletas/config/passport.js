const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');


module.exports = function(passport) {
    passport.use(new LocalStrategy({
        usernameField: 'email', // POR DEFECTO BUSCA "req.user" EN LUGAR DE ESO BUSCARA "req.email"
    },
        function(username, password, done) { //USERNAME ES EL NOMBRE POR DEFECTO --> SERA "email"
          Usuario.findOne({ email: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
              return done(null, false, { message: 'Incorrect username.' });
            }
            if (!user.validPassword(password)) {
              return done(null, false, { message: 'Incorrect password.' });
            }
            if (!user.verificado) {
                return done(null, false, { message: 'Cuenta no verificada' });
              }
            return done(null, user);
          });
        }
      ));

      passport.serializeUser(function(user, done) {
        done(null, user.id);
      });
      
      passport.deserializeUser(function(id, done) {
        Usuario.findById(id, function(err, user) {
          done(err, user);
        });
      });
}

//module.exports = passport;