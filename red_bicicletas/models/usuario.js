var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const uniqueValidator = require('mongoose-unique-validator');
const mailer = require('../mailer/mailer');
const Token = require('./token');
const crypto = require('crypto');

//FUNCIONES PARA LLAMAR EN EL SCHEMA
const validateEmail = function(email){
    const re = /(^\w+)*@\w+\.\w{2,3}/;
    return re.test(email);
}

const saltRounds = 10;

//SCHEMA DE USUARIO
var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true, // cuando se cree nombre se eliminaran posibles whitespaces a principio y fin
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true, //pasar a minuscula posibles mayusculas
        unique: true,
        validate: [validateEmail, 'Escribe un email valido' ],
        match: [/(^\w+)*@\w+\.\w{2,3}/]
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    _tokenID : {
        type: mongoose.Schema.Types.ObjectId, // Busca un token con este ID
        ref: 'Token'
    }
});

//La propiedad unique no es nativa de mongoose, luego se installa con npm y se añade como plugin
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} {VALUE} ya existe con otro usuario' }); 

usuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function validPassword(password){
    return bcrypt.compareSync(password, this.password);
};

// METODOS DEL MODELO USUARIO
usuarioSchema.methods.findNombreSimilar = function findNombreSimilar(cb){
    return this.model('Usuario').find({ nombre: this.nombre }, cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function enviar_email_bienvenida(cb){
    const token = new Token({_usuarioId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){ return console.log(err.message) }

        const mailOptions = {
            from: 'admin@pegaso.website',
            to: email_destination,
            subject: 'Verificacion de cuenta Quo Sabis',
            text: 'Hola,\n\n' + 'Por favor para verificar su cueta haga click en este link\n' + 'http://pegaso.website:8080' + '\/juego/token/confirmation\/' + token.token + '.\n'
        };
        // EMAIL DE CONFIRMACION
        mailer.sendMail(mailOptions, function(err, info){
            if(err){ return console.log(err.message) }
            console.log('Se ha enviado un email a ' + req.body.email + '.');
            console.log('email info ' + info.messageId  + '.');
        });
    });
    
    // LLAMAMOS AL PRPIO MODELO DENTRO DE SU METHOD --> "this.model('MI MODELO').funcionquesea"
    //AQUI LINKAMOS EL OBJETO TOKEN CON SU USUARIO CORRESPONDIENTE
    this.model('Usuario').findOneAndUpdate({ _id: this._id }, { _tokenID: token._id }, function(err, usuario){
        if(err){ 
            console.log(err);
        } else {
            console.log(usuario);
        };
    });
}

// STATICS DEL MODELO USUARIO
usuarioSchema.statics.buscar = function buscar(nombre, cb){
    return this.model('Usuario').find({ nombre: nombre }, cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);