var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true } //indice para agilizar las busquedas, indice de tipo coordenadas 2dsphere
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this ({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.statics.allBicis = function(cb){
    console.log("hola alguien me quiere");
    return this.find({}, cb); 
};



module.exports = mongoose.model('Bicicleta', bicicletaSchema);

// var Biclicleta = function(id, color, modelo, ubicacion){
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Biclicleta.prototype.toString = function(){
//     return 'id: ' + this.id + ' | color: ' + this.color; 
// }

// Biclicleta.allBicis = []; //ARRAY PARA GUARDAR BICICLETAS
// Biclicleta.add = function(aBici){
//     Biclicleta.allBicis.push(aBici);
// }

// Biclicleta.findById = function(aBiciId){
//     var aBici = Biclicleta.allBicis.find(x => x.id == aBiciId); //Se crea una variable que obtiente los objetos Bicicleta cuyos id sean igual al pasado como parametro
//     if(aBici){
//         return aBici;
//     } else {
//         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
//     }
// }

// Biclicleta.removeById = function(aBiciId){
//     var aBici = Biclicleta.findById(aBiciId);
//     for(var i = 0; i < Biclicleta.allBicis.length; i++){
//         if(Biclicleta.allBicis[i].id == aBiciId){
//             Biclicleta.allBicis.splice(i, 1); //elimina el elemento en esa posicioen de la lista
//             break;
//         }
//     }
// }


// var a = ;
// var b = new Biclicleta(2, 'azul', 'monty', [34.718892, -4.421970]);

// Biclicleta.add(a);
// Biclicleta.add(b);

// module.exports = Biclicleta;