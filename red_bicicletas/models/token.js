const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
    _usuarioId : {
        type: mongoose.Schema.Types.ObjectId, //Debe existir un usuario con este Id
        required: true,
        ref: 'Usuario'
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        index: { expires: '5s' }
    }
});

module.exports = mongoose.model('Token', tokenSchema);