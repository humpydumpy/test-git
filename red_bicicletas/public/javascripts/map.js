// La variable map pasa usando el id del div donde tenemos el mapa en el html y ademas pasa los valores de las coordenadas donde queremos que aparezca el mapa
var mymap = L.map('main_map').setView([36.718892, -4.421970], 13);

// Mapa open source
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//var marker = L.marker([36.718112, -4.421970]).addTo(mymap);
//var marker1 = L.marker([36.718892, -4.421974]).addTo(mymap);
//var marker2 = L.marker([36.722891, -4.421970]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.code}).addTo(mymap);
        });
    }
});

// var cuerpo = {
//         code: 12,
//         color: "verde",
//         modelo: "clasico",
//         lat: 12,
//         lgn: -22.2313
//     };

// console.log(JSON.stringify(cuerpo));
// $.ajax({
//     data: JSON.stringify( cuerpo ),
//     url: "api/bicicletas/create",
//     dataType: "json",
//     type: "POST",
//     contentType: "application/json",    
//     success: function(data){
//         console.log(data);
//     }
// });